package report;

import io.qameta.allure.Attachment;
import io.restassured.response.Response;
import org.json.simple.JSONObject;

public class ReportAttachment {

    /*
     *   This method attaches response body to Allure report
     */
    @Attachment(value = "responseBody", type = "text/json")
    public String attachResponseBody(Response response) {
        return response.getBody().asString();
    }

    @Attachment(value = "responseData", type = "text/json")
    public String attachFullResponseData(Response response) {
        Long responseTime = response.getTime();
        String headers = response.getHeaders().toString();
        String body = response.getBody().asString();
        int statusCode = response.getStatusCode();

        String responseData = String.format(
                "ResponseTime: %s millisec.\n" +
                        "StatusCode: %s\n" +
                        "Headers: %s\n" +
                        "Body: %s", responseTime, statusCode, headers, body);
        return responseData;
    }


    /*
     *   This method attaches parameters of the request to Allure report
     */
    @Attachment(value = "parametersOfRequest", type = "text/json")
    protected String attachParameters(JSONObject parameters) {
        return parameters.toString();
    }

    /*
     *   This method attaches parameters of the request to Allure report
     */
    @Attachment(value = "parametersOfRequest", type = "text/json")
    protected String attachParameters(Object parameters) {
        return parameters.toString();
    }

}

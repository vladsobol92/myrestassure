package responseProcessor;

import com.google.gson.Gson;
import helpers.Logger;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import static helpers.Logger.log;

public class ResponseProcessor {

    public static void logResponse(final Response response) {
        try {
            log("response: \n" + response.getBody().asString());
        } catch (Exception e) {
            Logger.logError("response: log FAILED");
        }
    }

    public static <T> T getResponsePath(Response response, String path) {
        T result = null;
        try {
            result = response.path(path);
        } catch (Exception ex) {
            Logger.logError("FAILED to get '" + path + "' from '" + response.body().asString() + "'");
        }
        return result;
    }


    public static <T> T getObjectFromResponse(Response response, Class<T> tClass) {
        // Step 1 -> get response body as hashMap
        HashMap resultAsHashMap = new HashMap();
        try {
            resultAsHashMap = response.as(HashMap.class);
        } catch (Exception ex) {
            Logger.logError("FAILED to convert response: " + response.body().asString() + "' into HashMap'");
            log(ex.getMessage());
        }
        // Step 2 -> Convert HashMap to JsonString
        String jsonString = new JSONObject(resultAsHashMap).toJSONString();
        // Step 3 -> Convert JsonString to Object of specified class
        T result = new Gson().fromJson(jsonString, tClass);

        return result;
    }

    public static <T> T getObjectFromResponsePath(Response response, String path, Class<T> tClass) {
        // Step 1 -> get response body as hashMap
        LinkedHashMap resultAsHashMap = getResponsePath(response, path);
        // Step 2 -> Convert HashMap to JsonString
        String jsonString = new JSONObject(resultAsHashMap).toJSONString();
        // Step 3 -> Convert JsonString to Object of specified class
        T result = new Gson().fromJson(jsonString, tClass);

        return result;
    }


    public static <T> List<T> getListFromResponse(Response response, String path, Class<T> tClass) {
        List<T> result = new ArrayList<>();
        try {
            result = response.jsonPath().getList(path, tClass);
        } catch (Exception ex) {
            log(ex.getMessage());
            Logger.logError("FAILED to get list '" + path + "' from '" + response.body().asString() + "'");
        }
        return result;
    }

    public static boolean isResponseStatusOK(Response response) {
        String result = getResponsePath(response, "status");
        return result != null && result.equals("OK");
    }

    public static void validateResponseStatusCode(Response response, int expectedCode) {
        int actualCode = response.getStatusCode();
        Assert.assertEquals(actualCode, expectedCode, "Response: status code incorrect");
    }


}

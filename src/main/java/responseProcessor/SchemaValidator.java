package responseProcessor;

import com.github.fge.jsonschema.SchemaVersion;
import com.github.fge.jsonschema.cfg.ValidationConfiguration;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.module.jsv.JsonSchemaValidatorSettings;
import io.restassured.response.Response;
import org.testng.Assert;

import java.io.File;


public class SchemaValidator {

    JsonSchemaFactory factory = JsonSchemaFactory.newBuilder()
            .setValidationConfiguration(
                    ValidationConfiguration.newBuilder()
                            .setDefaultVersion(SchemaVersion.DRAFTV4)
                            .freeze()).freeze();

    public void validateSchema(Response response, String pathToSchema) {
        File schemaFile = new File(pathToSchema);
        String responseBodyString = response.getBody().asString();
        // Check if responseBody NOT empty
        if (responseBodyString == null || responseBodyString.isEmpty()) {
            Assert.fail("FAILED to parse empty response");
        }
        response.then().assertThat()
                .body(JsonSchemaValidator.matchesJsonSchema(schemaFile)
                        .using(JsonSchemaValidatorSettings.settings()
                                .with()
                                .jsonSchemaFactory(factory)));
    }
}

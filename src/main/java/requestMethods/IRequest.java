package requestMethods;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public interface IRequest {

    Response perform(RequestSpecification requestSpec, String url, boolean showLogs);

}

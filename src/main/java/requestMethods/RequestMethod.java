package requestMethods;

public enum RequestMethod {
    GET,
    POST,
    PUT,
    DELETE
}


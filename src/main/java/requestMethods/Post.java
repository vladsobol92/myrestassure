package requestMethods;

import helpers.Logger;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static helpers.Sleeper.sleep;

public class Post implements IRequest {

    @Override
    public Response perform(RequestSpecification requestSpec, String url, boolean showLogs) {
        Response response = null;
        String responseBody;
        for (int i = 0; i < 2; i++) { // 2 times
            if (showLogs)
                response = RestAssured
                        .given().spec(requestSpec)
                        .log().all()
                        .when().post(url)
                        .then()
                        .log().all()
                        .extract().response();
            else
                response = RestAssured
                        .given().spec(requestSpec)
                        //.log().all()
                        .when().post(url)
                        .then()
                        // .log().all()
                        .extract().response();

            try {
                responseBody = response.getBody().asString();
                if (responseBody.contains("502 Proxy Error") || responseBody.contains("502 Bad Gateway")) {
                    sleep(1000);
                    Logger.logError("postRequest(): 502 error");
                    continue;
                }
            } catch (Exception e) {
                Logger.logError("Error receiving Post request: " + e.getMessage());
                e.printStackTrace();
            }
            break;
        }
        return response;
    }

}

package requestMethods;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Get implements IRequest {

    @Override
    public Response perform(RequestSpecification requestSpec, String url, boolean showLogs) {
        Response response;
        if (showLogs) {
            response = RestAssured
                    .given().spec(requestSpec)
                    .log().all()
                    .when().get(url)
                    .then()
                    .log().all()
                    .extract().response();
        } else {
            response = RestAssured
                    .given().spec(requestSpec)
                    //.log().all()
                    .when().get(url)
                    .then()
                    //.log().all()
                    .extract().response();
        }
        return response;
    }

}

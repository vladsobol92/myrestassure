package requestMethods;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RequestFactory {

    public static Response doRequest(RequestMethod method, RequestSpecification spec, String urlPath, boolean showLogs) {
        switch (method) {
            case GET:
            default:
                return new Get().perform(spec, urlPath, showLogs);
            case POST:
                return new Post().perform(spec, urlPath, showLogs);
            case PUT:
                return new Put().perform(spec, urlPath, showLogs);
            case DELETE:
                return new Delete().perform(spec, urlPath, showLogs);
        }
    }

    public static Response doRequest(RequestMethod method, RequestSpecification spec, String urlPath) {
        return doRequest(method, spec, urlPath, false);
    }
}

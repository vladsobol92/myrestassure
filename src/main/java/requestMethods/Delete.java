package requestMethods;

import helpers.Logger;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Delete implements IRequest {

    @Override
    public Response perform(RequestSpecification requestSpec, String url, boolean showLogs) {
        Response response = null;
        if (showLogs) {
            try {
                response = RestAssured
                        .given().spec(requestSpec)
                        .log().all()
                        .when().delete(url)
                        .then()
                        .log().all()
                        .extract().response();
            } catch (Exception e) {
                Logger.log("FAILED \n" + e.getMessage());
            }
        } else {
            try {
                response = RestAssured
                        .given().spec(requestSpec)
                        // .log().all()
                        .when().delete(url)
                        .then()
                        // .log().all()
                        .extract().response();
            } catch (Exception e) {
                Logger.log("FAILED \n" + e.getMessage());
            }
        }

        return response;
    }


}

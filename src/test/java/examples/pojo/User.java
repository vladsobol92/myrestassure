package examples.pojo;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {

    @SerializedName("gender")
    private String gender;

    @SerializedName("updated_at")
    private String updated_at;

    @SerializedName("name")
    private String name;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("id")
    private Integer id;

    @SerializedName("email")
    private String email;

    @SerializedName("status")
    private String status;

    public String toJSON() {
        return new Gson().toJson(this);
    }

    public String toJSONSkipNull() {
        return new Gson().toJson(this);
    }

}
package examples;

import examples.pojo.User;
import helpers.Logger;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.Test;
import requestMethods.RequestFactory;
import requestMethods.RequestMethod;
import responseProcessor.ResponseProcessor;

import java.util.HashMap;
import java.util.List;

public class getTest extends BaseTest {


    @Test
    public void test_get_and_validate_response_code() {
        String urlPath = "/users";

        RequestSpecification specification = new RequestSpecBuilder()
                .addRequestSpecification(base_specification)
                .build();

        Response response = RequestFactory.doRequest(RequestMethod.GET, specification, urlPath, true);
        ResponseProcessor.validateResponseStatusCode(response, 200);
    }


    @Test
    public void test_get_and_extract_list_as_map() {
        String urlPath = "/users";

        RequestSpecification specification = new RequestSpecBuilder()
                .addRequestSpecification(base_specification)
                .build();

        Response response = RequestFactory.doRequest(RequestMethod.GET, specification, urlPath);
        List<HashMap> listOfItems = ResponseProcessor.getListFromResponse(response, "data", HashMap.class);

        for (HashMap item : listOfItems) {
            Logger.log(item.get("email"));
        }
    }


    @Test
    public void test_get_and_extract_list_as_object() {
        String urlPath = "/users";

        RequestSpecification specification = new RequestSpecBuilder()
                .addRequestSpecification(base_specification)
                .build();

        Response response = RequestFactory.doRequest(RequestMethod.GET, specification, urlPath);
        List<User> listOfUsers = ResponseProcessor.getListFromResponse(response, "data", User.class);

        for (User user : listOfUsers) {
            Logger.log(user.toJSON());
        }
    }


    @Test
    public void test_get_and_extract_object_from_responsePath() {
        String userId = "1";
        String urlPath = String.format("/users/%s", userId);

        RequestSpecification specification = new RequestSpecBuilder()
                .addRequestSpecification(base_specification)
                .build();

        Response response = RequestFactory.doRequest(RequestMethod.GET, specification, urlPath, true);
        User user = ResponseProcessor.getObjectFromResponsePath(response, "data", User.class);
        Logger.log(user.toJSON());

    }

}

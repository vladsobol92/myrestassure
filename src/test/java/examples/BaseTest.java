package examples;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class BaseTest {
    protected String baseUrl = "https://gorest.co.in/public-api";
    protected String bearerToken = "0026070701ee265dac4deb186d053cfe92838539bd306ae77cead83bbfb845e5";
    protected RequestSpecification base_specification = new RequestSpecBuilder()
            .setBaseUri(baseUrl)
            .build();
}

package examples;

import examples.pojo.User;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import requestMethods.RequestFactory;
import requestMethods.RequestMethod;

public class postTests extends BaseTest {
    RequestSpecification baseSpec = new RequestSpecBuilder()
            .addRequestSpecification(base_specification)
            .setContentType(ContentType.JSON)
            .addHeader("Authorization", String.format("Bearer %s", bearerToken))
            .build();

    @Test
    public void post_body_as_JSONobject() {
        String urlPath = "/users";

        JSONObject user = new JSONObject();
        user.put("email", "vlad2@mail.test");
        user.put("name", "Vlad Sobol");
        user.put("gender", "Male");
        user.put("status", "Active");

        RequestSpecification spec = new RequestSpecBuilder()
                .addRequestSpecification(baseSpec)
                .setBody(user)
                .build();

        Response response = RequestFactory.doRequest(RequestMethod.POST, spec, urlPath, true);

    }

    @Test
    public void post_body_as_Object() {
        String urlPath = "/users";

        User user = new User();
        user.setEmail("vlad3@mail.test");
        user.setName("Vlad Sobol");
        user.setGender("Male");
        user.setStatus("Active");

        RequestSpecification spec = new RequestSpecBuilder()
                .addRequestSpecification(baseSpec)
                .setBody(user)
                .build();

        Response response = RequestFactory.doRequest(RequestMethod.POST, spec, urlPath, true);

    }
}
